import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    data_with_titles = df[df['Name'].str.contains('Mr\.|Mrs\.|Miss\.', regex=True, na=False)]


    title_groups = data_with_titles.groupby(data_with_titles['Name'].str.extract('(Mr\.|Mrs\.|Miss\.)')[0])['Age']
    missed_values_counts = title_groups.apply(lambda x: x.isnull().sum())
    median_values = title_groups.median().round()

    answer = [('Mr.', missed_values_counts['Mr.'], median_values['Mr.']),
              ('Mrs.', missed_values_counts['Mrs.'], median_values['Mrs.']),
              ('Miss.', missed_values_counts['Miss.'], median_values['Miss.'])]

    return answer
